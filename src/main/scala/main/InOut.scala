package main

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.json4s.JValue

import java.io.{BufferedReader, InputStreamReader}
import java.net.URI
import java.util.Properties
import scala.collection.immutable.Stream

object InOut {

  def read(spark: SparkSession, filePath: String, format: String): DataFrame = {
    spark
      .read
      .options(Map("inferschema" -> "true", "header" -> "true", "delimiter" -> ";"))
      .format(format)
      .load(filePath)
      .repartition(8)
      .cache()
  }

  def readFile(path: String, url: String = "", encoding: String = "UTF-8"): String = {
    val conf = new Configuration()
    val fs = url match {
      case url if url.startsWith("file://") | url == "" => FileSystem.getLocal(conf).getRawFileSystem
      case _ => FileSystem.get(new URI(url), conf)
    }
    val stream = fs.open(new Path(path))
    val br = new BufferedReader(new InputStreamReader(stream, encoding))
    try {
      Stream.continually(br.readLine).takeWhile(_ != null).mkString
    }
    finally {
      br.close()
      stream.close()
    }
  }

  def write(data: DataFrame, filePath: String, partitionBy: String = ""): Unit = {
    val writer = data
      .repartition(1)
      .write
      .mode(SaveMode.Overwrite)
    partitionBy match {
      case "" => writer
        .parquet(filePath)
      case _ => writer
        .partitionBy(partitionBy)
        .parquet(filePath)
    }
  }

  def writeToDataBase(jsonPath: String, df: DataFrame, tableName: String): Unit = {
    val conf = ParseJson
      .parseJson(jsonPath)
    val connectionURL = ParseJson
      .getJsonValue(conf, "connectionURL")
    val user = ParseJson
      .getJsonValue(conf, "user")
    val password = ParseJson
      .getJsonValue(conf, "password")
    val schema = ParseJson
      .getJsonValue(conf, "schema")

    val connectionProperties = new Properties()
    connectionProperties.put("user", user)
    connectionProperties.put("password", password)
    connectionProperties.put("driver", "org.postgresql.Driver")

    df
      .write
      .mode(SaveMode.Overwrite)
      .jdbc(connectionURL, s"$schema.$tableName", connectionProperties)
  }

  def writeToDataBase(json: JValue, df: DataFrame, tableName: String): Unit = {
    val connectionURL = ParseJson
      .getJsonValue(json, "connectionURL")
    val user = ParseJson
      .getJsonValue(json, "user")
    val password = ParseJson
      .getJsonValue(json, "password")
    val schema = ParseJson
      .getJsonValue(json, "schema")

    val connectionProperties = new Properties()
    connectionProperties.put("user", user)
    connectionProperties.put("password", password)
    connectionProperties.put("driver", "org.postgresql.Driver")

    df
      .write
      .mode(SaveMode.Overwrite)
      .jdbc(connectionURL, s"$schema.$tableName", connectionProperties)
  }
}
