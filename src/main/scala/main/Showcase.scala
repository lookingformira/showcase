package main

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{coalesce, col, not, rank, round, sum, when}
import org.apache.spark.sql.types.DecimalType

object Showcase {

  def buildCorporatePayments(dfOperations: DataFrame,
                       dfRate: DataFrame,
                       dfAccounts: DataFrame,
                       dfClients: DataFrame,
                       dfTech: DataFrame): DataFrame = {

    //датафрейм джойна таблиц операций и курса валют
    val dfOpRate = dfOperations
      .join(dfRate)
      .where(dfOperations("Currency") === dfRate("Currency"))
      .where(dfRate("RateDate").<=(dfOperations("DateOp")))
      .cache()

    //формирование фрейма, в котором все операции в RUB
    val windowSpec = Window
      .partitionBy("AccountDB", "AccountCR", "DateOp", "Amount", "Currency")
      .orderBy(col("RateDate").desc)

    val dfRub = dfOpRate
      .select(col("AccountDB"), col("AccountCR"), col("Comment"),
        dfOperations("Currency"), col("Amount"), col("Rate"),
        col("DateOp"), col("Rate"), col("RateDate"))
      .withColumn("rt", rank().over(windowSpec))
      .where(col("rt") === 1)
      .withColumn("AmountRub", round(col("Amount") * col("Rate"), 2))
      .cache()

    //формирование фрейма дебета, используя джойны для создания витрины
    val dfDb = dfRub
      .join(dfAccounts)
      .where(dfRub("AccountCR") === dfAccounts("AccountID"))
      .join(dfClients)
      .where(dfClients("ClientId") === dfAccounts("ClientId"))
      .select(col("AccountDB").as("AccountId"), col("AccountCR"),
        col("DateOp"), col("AmountRub"), col("Comment"),
        col("Type"), dfAccounts("AccountNum"), col("AmountRub").as("Amount"))
      .cache()

    //список 1
    val stringCars = dfTech.select("product").where(col("id").<(200))
      .rdd.map(row => row(0)).collect().mkString("|").replace("%", "\\b")
      .replace(" ", "")

    //датафрейм для суммы, где счет в дебете
    val dfDbSum = dfDb.groupBy(col("AccountId"), col("DateOp").as("CutoffDt"))
      .agg(
        round(sum(col("Amount")), 2) as "PaymentAmt",
        round(sum(when(col("AccountNum").like("40702%"), col("Amount")).otherwise(0)), 2)
          .as("TaxAmt"),
        round(sum(when(not(col("Comment").rlike(s"\\b$stringCars\\b")), col("Amount")).otherwise(0)), 2)
          .as("CarsAmt"),
        round(sum(when(col("Type").===("Ф"), col("Amount")).otherwise(0)), 2)
          .as("FLAmt"))
      .cache()

    //формирование фрейма кредита, используя джойны для создания витрины
    val dfCr = dfRub
      .join(dfAccounts)
      .where(dfRub("AccountDB") === dfAccounts("AccountID"))
      .select(col("AccountCR").as("AccountId"), col("AccountDB"),
        col("DateOp"), col("AmountRub"), col("Comment"),
        dfAccounts("AccountNum"), col("AmountRub").as("Amount"))
      .cache()

    //список 2
    val stringFood = dfTech.select("product").where(col("id").>=(200))
      .rdd.map(row => row(0)).collect().mkString("|").replace("%", "\\b")
      .replace(" ", "")

    //датафрейм для суммы, где счет в кредите
    val dfCrSum = dfCr.groupBy(col("AccountId"),
      col("DateOp").as("CutoffDt"))
      .agg(
        round(sum(col("Amount")), 2).as("EnrollementAmt"),
        round(sum(when(col("AccountNum").like("40802%"), col("Amount")).otherwise(0)), 2)
          .as("ClearAmt"),
        sum(when(col("Comment").rlike(s"\\b$stringFood\\b"), col("Amount")).otherwise(0))
          .cast(DecimalType(9,2)).as("FoodAmt") // спросить про round и cast
      )
      .cache()

    //витрина 1
    val preCorporatePayments = dfDbSum.join(dfCrSum, dfDbSum("AccountId") === dfCrSum("AccountId") &&
      dfDbSum("CutoffDt") === dfCrSum("CutoffDt"), "fullouter")
      .select(coalesce(dfCrSum("AccountId"), dfDbSum("AccountId")).as("AccountId"),
        dfDbSum("PaymentAmt"), dfCrSum("EnrollementAmt"), dfDbSum("TaxAmt"),
        dfCrSum("ClearAmt"), dfDbSum("CarsAmt"), dfCrSum("FoodAmt"), dfDbSum("FLAmt"),
        coalesce(dfDbSum("CutoffDt"), dfCrSum("CutoffDt")).as("CutoffDt"))
      .na.fill(0)
      .cache()

    preCorporatePayments
      .join(dfAccounts, dfAccounts("AccountID") === preCorporatePayments("AccountId"))
      .select(
        preCorporatePayments("AccountId") as "AccountID",
        dfAccounts("ClientId"),
        preCorporatePayments("PaymentAmt"),
        preCorporatePayments("EnrollementAmt"),
        preCorporatePayments("TaxAmt"),
        preCorporatePayments("ClearAmt"),
        preCorporatePayments("CarsAmt"),
        preCorporatePayments("FoodAmt"),
        preCorporatePayments("FLAmt"),
        preCorporatePayments("CutoffDt")
      )
      .cache()
  }

  def buildCorporateAccount(dfClients: DataFrame,
                            dfAccounts: DataFrame,
                            corporatePayments: DataFrame): DataFrame = {
    val dfClientsModified = dfClients
      .withColumnRenamed("ClientId", "ClientId_renamed")
      .withColumnRenamed("ClientName", "ClientName_renamed")
      .withColumnRenamed("Type", "Type_renamed")
      .withColumnRenamed("Form", "Form_renamed")
      .withColumnRenamed("RegisterDate", "RegisterDate_renamed")
      .cache()

    val dfAccountsModified = dfAccounts
      .withColumnRenamed("AccountID", "AccountID_renamed")
      .withColumnRenamed("AccountNum", "AccountNum_renamed")
      .withColumnRenamed("ClientId", "ClientId_renamed")
      .withColumnRenamed("DateOpen", "DateOpen_renamed")
      .cache()

    val corporateAccount = corporatePayments
      .join(dfAccountsModified, corporatePayments("AccountID") === dfAccountsModified("AccountID_renamed"))
      .join(dfClientsModified, dfClientsModified("ClientId_renamed") === dfAccounts("ClientId"))
      .select(
        corporatePayments("AccountId").as("AccountID"),
        dfAccountsModified("AccountNum_renamed").as("AccountNum"),
        dfAccountsModified("DateOpen_renamed").as("DateOpen"),
        corporatePayments("ClientId"),
        dfClientsModified("ClientName_renamed").as("ClientName"),
        (corporatePayments("PaymentAmt") + corporatePayments("EnrollementAmt")).as("TotalAmt"),
        corporatePayments("CutoffDt")
      )
      //исправление
      .groupBy("AccountID", "CutoffDt", "AccountNum", "DateOpen", "ClientId", "ClientName")
      .agg(sum("TotalAmt").as("TotalAmt"))
      .cache()

    corporateAccount
      .select("AccountID", "AccountNum", "DateOpen", "ClientId", "ClientName", "TotalAmt", "CutoffDt")
      .cache()
  }

  def buildCorporateInfo(corporateAccount: DataFrame, dfClients: DataFrame): DataFrame = {
    val corporateInfo = corporateAccount
      .join(dfClients, dfClients("ClientId") === corporateAccount("ClientId"))
      .select(
        corporateAccount("ClientId"),
        corporateAccount("ClientName"),
        dfClients("Type"),
        dfClients("Form"),
        dfClients("RegisterDate"),
        corporateAccount("TotalAmt"),
        corporateAccount("CutoffDt")
      )
      .groupBy(
        "ClientId", "ClientName", "Type", "Form", "RegisterDate", "CutoffDt"
      )
      .agg(sum("TotalAmt").as("TotalAmt"))
      .cache()

    corporateInfo
      .select("ClientId", "ClientName", "Type", "Form", "RegisterDate", "TotalAmt", "CutoffDt")
      .cache()
  }
}
