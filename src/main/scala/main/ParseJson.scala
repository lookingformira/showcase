package main

import org.json4s._
import org.json4s.jackson.JsonMethods.parse

import scala.io.{BufferedSource, Source}

object ParseJson {

  def parseJson(filePath: String): JValue = {
    var jsonSrc: BufferedSource = null
    try {
      jsonSrc = Source.fromFile(filePath)
      val str = jsonSrc.mkString
      parse(str)
    } finally {
      jsonSrc.close
    }
  }

  def parseJson(filePath: String, url: String): JValue = {
    val jsonSrc = InOut.readFile(filePath, url)
    parse(jsonSrc)
  }

  def getJsonValue(parsedJson: JValue, key: String): String = {
    val JString(value) = parsedJson \ key
    value
  }
}
