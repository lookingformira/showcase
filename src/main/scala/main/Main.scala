package main

import org.apache.spark.sql.SparkSession

object Main extends App {

  val spark: SparkSession = SparkSession
    .builder()
    .master("local[4]")
    .getOrCreate()

  val fileSystem = args(0)
  val configPath = args(1)

  val json = ParseJson.parseJson(configPath, s"$fileSystem")
  val csvPath = ParseJson.getJsonValue(json, "csvPath")
  val parquetPath = ParseJson.getJsonValue(json, "parquetPath")

  val dfAccount = InOut.read(spark, s"$csvPath/bills.csv", "csv")
  val dfClient = InOut.read(spark,  s"$csvPath/clients.csv", "csv")
  val dfRate = InOut.read(spark, s"$csvPath/course.csv", "csv")
  val dfOperation = InOut.read(spark, s"$csvPath/operationsUTF.csv", "csv")
  val dfTech = InOut.read(spark, s"$csvPath/calculation_params_tech.csv", "csv")

  InOut.write(dfAccount, s"$parquetPath/samples/Account", "DateOpen")
  InOut.write(dfClient, s"$parquetPath/samples/Client", "RegisterDate")
  InOut.write(dfRate, s"$parquetPath/samples/Rate", "RateDate")
  InOut.write(dfOperation, s"$parquetPath/samples/Operation", "DateOp")
  InOut.write(dfTech, s"$parquetPath/samples/calculation_params_tech")

  val corporatePayments = Showcase
    .buildCorporatePayments(dfOperation, dfRate, dfAccount, dfClient, dfTech)
  val corporateAccount = Showcase
    .buildCorporateAccount(dfClient, dfAccount, corporatePayments)
  val corporateInfo = Showcase
    .buildCorporateInfo(corporateAccount, dfClient)

  InOut
    .write(corporatePayments,
      s"$parquetPath/showcase/sparkAPI/Part/corporate_payments",
      "CutoffDt")
  InOut
    .write(corporateAccount,
      s"$parquetPath/showcase/sparkAPI/Part/corporate_account",
      "CutoffDt")
  InOut
    .write(corporateInfo,
      s"$parquetPath/showcase/sparkAPI/Part/corporate_info",
      "CutoffDt")

  InOut.writeToDataBase(json, corporatePayments, "corporate_payments")
  InOut.writeToDataBase(json, corporateAccount, "corporate_account")
  InOut.writeToDataBase(json, corporateInfo, "corporate_info")
}
